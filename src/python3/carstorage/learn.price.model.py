from easy_search.core.server.index.manager.elasticsearch.ElasticDocumentManager import ElasticDocumentManager

from carstorage.index.Document import Document
from carstorage.index.DocumentQueue import DocumentQueue
from carstorage.learning.regression.IndexLinearRegression import IndexLinearRegression
from carstorage.learning.regression.IndexMultiLayeredPerceptron import IndexMultiLayeredPerceptron
from carstorage.learning.regression.IndexSVR import IndexSVR
from carstorage.storage.CacheProvider import CacheProvider

elastic = 'http://localhost:9200'

if __name__ == '__main__':
    provider = CacheProvider('var/cache')
    features = provider.get('features')
    if features is None:
        raise Exception("Features must be extracted before learning models!")
    queue = DocumentQueue(elastic)
    #model = IndexLinearRegression(queue, features, 'price')
    #model = IndexMultiLayeredPerceptron(queue, features, 'price')
    model = IndexSVR(queue, features, 'price')
    error_curve, error, learning_time = model.test_with_mean_squared_error(0.33, 10000)
    print('Learning MSE: %f' % error)
    print('Learning time: %f' % learning_time)

    model.train()
    provider.put('regression_model', model.model)


