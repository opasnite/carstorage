from typing import Optional

from easy_search.core.base.dependency.service import json_serializer
from elasticsearch import Elasticsearch
from elasticsearch.helpers import scan

from carstorage.index.Document import Document


class DocumentQueue:
    def __init__(self, elastic_connection: str, cluster: int = None) -> None:
        self.cluster = cluster
        self.manager = Elasticsearch([elastic_connection])
        self.query = None
        if self.cluster is not None:
            self.query = {
                "query": {
                    "match": {
                        "cluster": self.cluster
                    }
                }
            }
        self.queue = scan(self.manager, index='carstorage', preserve_order=True, query=self.query)
        self.serialize = json_serializer()
        count_response = self.manager.search('carstorage', 'index', {"query": {"match_all": {}}, "size": 0})
        self.document_count = count_response['hits']['total']

    def reset(self):
        self.queue = scan(self.manager, index='carstorage', preserve_order=True, query=self.query)

    def get_next_document(self) -> Optional[Document]:
        try:
            value = next(self.queue)
        except StopIteration:
            return None
        return self.serialize.deserialize(value['_source'], Document)
