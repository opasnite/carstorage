from datetime import date, datetime
from typing import List

from easy_search.interfaces.server.index.communication.common.IndexDocument import IndexDocument


class Document(IndexDocument):

    def __init__(self, unique_id: str, title: str, url: str, description: str = '', price: float = None,
                 currency: str = None, year: datetime = None, kilometers: int = None, keywords: str = '',
                 cluster: int = None, predicted_price: float = None) -> None:
        super().__init__(unique_id)
        self.title = title
        self.keywords = keywords
        self.description = description
        self.url = url
        self.price = price
        self.currency = currency
        self.year = year
        self.kilometers = kilometers
        self.cluster = cluster
        self.predicted_price = predicted_price

    def add_keywords(self, words: List[str]) -> None:
        self.keywords += ','.join(words)
