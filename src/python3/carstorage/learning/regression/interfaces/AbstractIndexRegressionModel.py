import time
from abc import ABC
from math import floor
from typing import List

from carstorage.index.DocumentQueue import DocumentQueue
from carstorage.learning.features.types.Feature import Feature
from carstorage.learning.regression.interfaces.IndexRegressionModelInterface import IndexRegressionModelInterface
from carstorage.learning.utils.DocumentHelper import DocumentHelper


class AbstractIndexRegressionModel(IndexRegressionModelInterface, ABC):
    def __init__(self, queue: DocumentQueue, features: List[Feature], target: str) -> None:
        self.queue = queue
        self.doc_helper = DocumentHelper(features.copy())
        for feature in features:
            if feature.name == target:
                features.remove(feature)
        self.__features = features
        self.feature_count = len(features)
        self.target = target

    def test_with_mean_squared_error(self, test_size: float = 0.33, max_document: int = None):
        train_size = 1 - test_size
        start_time = time.time()
        error_curve = self.train(train_size)
        error = 0
        count = 0
        if max_document is not None:
            max_document = floor(test_size * max_document)
        document = self.queue.get_next_document()
        while document is not None:
            count += 1
            train_x, train_y = self.doc_helper.convert_for_regression(document, self.target)
            if len(train_x) == 0:
                document = self.queue.get_next_document()
                continue
            predicted = self.predict(document)
            error += abs(predicted - train_y[0])
            document = self.queue.get_next_document()
        error /= count
        return error_curve, error, time.time() - start_time
