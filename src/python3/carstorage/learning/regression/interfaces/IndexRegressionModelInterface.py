from abc import ABC, abstractmethod
from typing import List

from carstorage.index.Document import Document
from carstorage.index.DocumentQueue import DocumentQueue


class IndexRegressionModelInterface(ABC):
    @abstractmethod
    def train(self, train_percent: float = 1) -> list: raise NotImplementedError

    @abstractmethod
    def load(self, params) -> None: raise NotImplementedError

    @abstractmethod
    def predict(self, doc: Document) -> float: raise NotImplementedError

    @abstractmethod
    def test_with_mean_squared_error(self, test_size: float = 0.33): raise NotImplementedError
