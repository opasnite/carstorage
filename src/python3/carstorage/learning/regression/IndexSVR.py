from typing import List

import numpy
from sklearn.svm import SVR

from carstorage.index.Document import Document
from carstorage.index.DocumentQueue import DocumentQueue
from carstorage.learning.features.types.Feature import Feature
from carstorage.learning.regression.interfaces.AbstractIndexRegressionModel import AbstractIndexRegressionModel


class IndexSVR(AbstractIndexRegressionModel):
    def __init__(self, queue: DocumentQueue, features: List[Feature], target: str) -> None:
        super().__init__(queue, features, target)

        self.model = SVR(gamma='auto')

    def train(self, train_percent: float = 1) -> list:
        self.queue.reset()
        count = 10000
        if self.queue.document_count < count:
            count = self.queue.document_count
        max_document = round(count * train_percent)
        doc_read = 0
        document = self.queue.get_next_document()
        batch_x = []
        batch_y = []
        while document is not None and doc_read < max_document:
            train_x, train_y = self.doc_helper.convert_for_regression(document, self.target)
            if len(train_x) == 0:
                document = self.queue.get_next_document()
                continue
            doc_read += 1
            batch_x.append(train_x)
            batch_y.append(train_y)
            document = self.queue.get_next_document()
        self.model.fit(batch_x, numpy.ravel(batch_y, order='C'))
        error_result = self.model.score(batch_x, batch_y)
        print("BATCH SCORE: %f" % error_result)
        return [error_result]

    def load(self, params) -> None:
        self.model = params

    def predict(self, doc: Document) -> float:
        train_x, train_y = self.doc_helper.convert_for_regression(doc, self.target)
        return self.model.predict([train_x])
