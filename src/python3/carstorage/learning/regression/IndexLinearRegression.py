import random
from typing import List

import numpy
from sklearn.linear_model import SGDRegressor

from carstorage.index.Document import Document
from carstorage.index.DocumentQueue import DocumentQueue
from carstorage.learning.features.types.Feature import Feature
from carstorage.learning.regression.interfaces.AbstractIndexRegressionModel import AbstractIndexRegressionModel


class IndexLinearRegression(AbstractIndexRegressionModel):
    def __init__(self, queue: DocumentQueue, features: List[Feature], target: str) -> None:
        super().__init__(queue, features, target)
        self.model = SGDRegressor()

    def train(self, train_percent: float = 1) -> list:
        self.queue.reset()
        count = self.queue.document_count
        max_document = round(count * train_percent)
        doc_read = 0
        document = self.queue.get_next_document()
        batch_size = 10000
        batch_x = []
        batch_y = []
        error = []
        while document is not None and doc_read < max_document:
            doc_read += 1
            train_x, train_y = self.doc_helper.convert_for_regression(document, self.target)
            if len(train_x) == 0:
                document = self.queue.get_next_document()
                continue
            batch_x.append(train_x)
            batch_y.append(train_y)
            if len(batch_x) >= batch_size or doc_read == max_document:
                self.model.partial_fit(batch_x, numpy.ravel(batch_y, order='C'))
                error_result = self.model.score(batch_x, batch_y)
                print("BATCH SCORE: %f" % error_result)
                batch_x = []
                batch_y = []
                error.append(error_result)
            document = self.queue.get_next_document()
        return error

    def load(self, params) -> None:
        self.model = params

    def predict(self, doc: Document) -> float:
        train_x, train_y = self.doc_helper.convert_for_regression(doc, self.target)
        return self.model.predict([train_x])
