from abc import ABC, abstractmethod


class IIndexClustering(ABC):
    @abstractmethod
    def run_clustering(self, centroids: list = None) -> list: raise NotImplementedError
