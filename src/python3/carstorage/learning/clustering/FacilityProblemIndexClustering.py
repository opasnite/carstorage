from math import sqrt, log, inf, floor
from typing import List, Tuple, Optional

from elasticsearch import Elasticsearch
from numpy import inner
from numpy.ma import array
from numpy.random import rand
from scipy.spatial.distance import euclidean
from sklearn.cluster import KMeans

from carstorage.index.DocumentQueue import DocumentQueue
from carstorage.learning.clustering.Facility import Facility
from carstorage.learning.clustering.interfaces.IIndexClustering import IIndexClustering
from carstorage.learning.features.types.Feature import Feature
from carstorage.learning.utils.DocumentHelper import DocumentHelper


class FacilityProblemIndexClustering(IIndexClustering):
    def __init__(self, elastic_connection: str, features: List[Feature]) -> None:
        super().__init__()
        self.manager = Elasticsearch([elastic_connection])
        self.queue = DocumentQueue(elastic_connection)
        self.features = features
        self.doc_helper = DocumentHelper(features.copy())
        document_count = self.queue.document_count
        document_count_ln = log(document_count)
        self.cluster_count = round(sqrt(document_count))
        self.facility_cost = (1 / self.cluster_count) * (1 + document_count_ln)
        self.maximum_facilities = round(self.cluster_count * document_count_ln)
        self.model = KMeans(self.cluster_count)
        self.facility_cache = {}
        self.facility_cache_keys = []

    def __find_approximate_nn(self, facilities: List[Facility], document: list, ann_point: float) \
            -> Tuple[float, Optional[Facility], int]:
        minimum_distance = float(inf)
        closest_facility = None
        facility_import_key = 0
        previous_facility = None
        ann_cache_entry = floor(ann_point * 1000)
        for cache_group_key in self.facility_cache_keys:
            if cache_group_key < ann_cache_entry:
                continue
            for key in self.facility_cache[cache_group_key]:
                facility = facilities[key]
                if facility.ann_value > ann_point:
                    current_distance = euclidean(facility.current_center, document)
                    if closest_facility is None:
                        minimum_distance = current_distance
                        closest_facility = facility
                    else:
                        top = current_distance
                        bottom = euclidean(previous_facility.current_center, document)
                        minimum_distance = min(top, bottom)
                        if minimum_distance == bottom:
                            closest_facility = previous_facility
                        else:
                            closest_facility = facility
                    facility_import_key = key
                    break
                previous_facility = facility
            if closest_facility is not None:
                break
        if closest_facility is None and len(facilities) > 0:
            closest_facility = facilities[len(facilities) - 1]
            minimum_distance = euclidean(closest_facility.current_center, document)
            facility_import_key = len(facilities)
        return minimum_distance, closest_facility, facility_import_key

    def __find_best_facilities(self, facility_cost: float, max_facilities: int, progress_speed: float = 3.0):
        facilities = []
        random_seed = rand(len(self.features))
        document = self.queue.get_next_document()
        document_read = 0
        while document is not None:
            while document is not None and len(facilities) <= max_facilities:
                converted_document = self.doc_helper.convert_for_clustering(document)
                if len(converted_document) <= 0:
                    document = self.queue.get_next_document()
                    continue
                ann_point = inner(converted_document, random_seed)
                minimum_distance, closest_facility, facility_import_key = self.__find_approximate_nn(facilities,
                                                                                                     converted_document,
                                                                                                     ann_point)
                if min(minimum_distance / facility_cost, 1) == 1:
                    new_facility = Facility(converted_document, random_seed.tolist())
                    facilities[facility_import_key: facility_import_key + 0] = [new_facility]
                    ann_facility = floor(new_facility.ann_value * 1000)
                    if ann_facility not in self.facility_cache:
                        self.facility_cache[ann_facility] = []
                        self.facility_cache_keys.append(ann_facility)
                        self.facility_cache_keys.sort()
                    self.facility_cache[ann_facility].append(facility_import_key)
                    if len(facilities) % 100 == 0:
                        print("Facilities count: %d" % len(facilities))
                else:
                    closest_facility.add_inner_point(converted_document)
                document = self.queue.get_next_document()
                document_read += 1
            print("Documents read: %d" % document_read)
            while len(facilities) > max_facilities:
                facility_cost *= progress_speed
                self.facility_cache = {}
                self.facility_cache_keys = []
                for facility in facilities:
                    facility.recenter()
                facilities.sort(key=lambda x: x.ann_value)
                starting_facility = facilities.pop()
                new_facilities = [starting_facility]
                ann_facility = floor(starting_facility.ann_value * 1000)
                self.facility_cache[ann_facility] = [0]
                self.facility_cache_keys.append(ann_facility)
                for facility in facilities:
                    minimum_distance, closest_facility, facility_import_key = \
                        self.__find_approximate_nn(new_facilities, facility.current_center, facility.ann_value)
                    if min(facility.inner_point_count * minimum_distance / facility_cost, 1) == 1:
                        new_facilities[facility_import_key: facility_import_key + 0] = [facility]
                        ann_facility = floor(facility.ann_value * 1000)
                        if ann_facility not in self.facility_cache:
                            self.facility_cache[ann_facility] = []
                            self.facility_cache_keys.append(ann_facility)
                            self.facility_cache_keys.sort()
                        self.facility_cache[ann_facility].append(facility_import_key)
                    else:
                        closest_facility.add_inner_point(facility.current_center)
                facilities = new_facilities
        return facilities

    def __learn(self, points: list = None):
        if points is None:
            facilities = self.__find_best_facilities(self.facility_cost, self.maximum_facilities)
            points = []
            for facility in facilities:
                points.append(facility.current_center)
            points = array(points)
        if len(points) <= self.cluster_count:
            self.model.cluster_centers_ = points
        else:
            self.model.fit(points)

    def __assign_index_to_clusters(self):
        document = self.queue.get_next_document()
        while document is not None:
            converted_document = self.doc_helper.convert_for_clustering(document)
            if len(converted_document) <= 0:
                document = self.queue.get_next_document()
                continue
            centroid = self.model.predict([converted_document])
            print("%s: %d" % (document.unique_id, centroid[0]))
            query = {
                "script": {
                    "source": "ctx._source.cluster=params.cluster",
                    "lang": "painless",
                    "params": {
                        "cluster": int(centroid[0])
                    }
                },
                "query": {
                    "term": {
                        "unique_id": document.unique_id
                    }
                }
            }
            self.manager.update_by_query('carstorage', 'index', query)
            document = self.queue.get_next_document()

    def run_clustering(self, centroids: list = None) -> list:
        self.__learn(centroids)
        self.queue.reset()
        self.__assign_index_to_clusters()
        return self.model.cluster_centers_

