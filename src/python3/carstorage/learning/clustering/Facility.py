from typing import Iterable

from numpy.ma import dot


class Facility:
    def __init__(self, center: list, seed: Iterable) -> None:
        self.new_center_sum = center
        self.seed = seed
        self.inner_point_count = 1
        self.current_center = center.copy()
        self.ann_value = dot(seed, center)

    def add_inner_point(self, point: list):
        for key in range(0, len(self.new_center_sum)):
            self.new_center_sum[key] += point[key]
        self.inner_point_count += 1

    def recenter(self):
        if self.inner_point_count == 0:
            return
        for key in range(0, len(self.new_center_sum)):
            self.current_center[key] = self.new_center_sum[key] / self.inner_point_count
        self.ann_value = dot(self.seed, self.current_center)
