from math import ceil, floor
from typing import List, Optional
from elasticsearch import Elasticsearch

from carstorage.index.DocumentQueue import DocumentQueue
from carstorage.index.exception.CommunicationException import CommunicationException
from carstorage.learning import features
from carstorage.learning.features.types.BooleanNumericFeature import BooleanNumericFeature
from carstorage.learning.features.types.BooleanTextFeature import BooleanTextFeature
from carstorage.learning.features.types.Feature import Feature
from carstorage.learning.features.types.NumericFeature import NumericFeature
from carstorage.learning.features.types.NumericFeatureCharacteristics import NumericFeatureCharacteristics


class IndexFeatureExtractor:
    def __init__(self, elastic_connection: str, word_support_percent: float = 0.4) -> None:
        self.numeric_features = []
        self.text_features = []
        self.__word_cache = set([])
        self.search = Elasticsearch([elastic_connection])
        self.queue = DocumentQueue(elastic_connection)
        count_response = self.search.search('carstorage', 'index', {"query": {"match_all": {}}, "size": 0})
        self.document_count = count_response['hits']['total']
        self.min_document_support = word_support_percent * self.document_count

    def add_numeric_feature(self, feature: str):
        self.numeric_features.append(feature)

    def add_text_feature(self, feature: str):
        self.text_features.append(feature)

    def __fetch_feature_value_at_position(self, feature: str, position: float, order_type: str = 'asc'):
        query = {
            "query": {"match_all": {}},
            "size": 1,
            "sort": {feature: order_type},
            "from": position
        }
        try:
            document = self.search.search('carstorage', 'index', query)
            if 'hits' not in document:
                raise CommunicationException('Document request returned unexpected response!')
            return document['hits']['hits'][0]['sort'][0]
        except Exception as e:
            raise CommunicationException('Could not fetch document from position!', e)

    def __get_feature_value_at_position(self, feature: str, position: float, order_type: str = 'asc'):
        if position > 9998.5:
            position = 9998.5
        if floor(position) == position:
            value = self.__fetch_feature_value_at_position(feature, position, order_type)
        else:
            bottom = floor(position)
            high = ceil(position)
            bottom_value = self.__fetch_feature_value_at_position(feature, bottom, order_type)
            high_value = self.__fetch_feature_value_at_position(feature, high, order_type)
            value = (bottom_value + high_value) / 2
        return value

    def __load_numeric_characteristics(self, feature: str) -> NumericFeatureCharacteristics:
        first_quartile_position = (self.document_count + 1) / 4
        third_quartile_position = self.document_count - 3 * first_quartile_position
        first_quartile_value = self.__get_feature_value_at_position(feature, first_quartile_position, 'asc')
        third_quartile_value = self.__get_feature_value_at_position(feature, third_quartile_position, 'desc')
        return NumericFeatureCharacteristics(first_quartile_value, third_quartile_value)

    def __split(self, text: str) -> list:
        # Dirty workaround to not load regex library
        return text.replace(';', ' ').replace(',', ' ').split()

    def __normalize(self, word: str) -> str:
        return word.strip().replace('\\', "").replace('"', "").replace("'", "")

    def __is_numeric(self, word: str) -> bool:
        # Dirty workaround to not load regex library
        return word.replace('.', '', 1).isdigit()

    def __get_feature_from_word(self, feature_name: str, word: str) -> Optional[Feature]:
        if len(word) <= 2:
            return None
        if self.__is_numeric(word):
            return None
        if word in self.__word_cache:
            return None
        self.__word_cache.add(word)
        count_response = self.search.search('carstorage', 'index', {"query": {"match": {feature_name: word}}, "size": 0})
        word_document_count = count_response['hits']['total']
        if word_document_count < self.min_document_support or \
                self.document_count - word_document_count < self.min_document_support:
            return None
        return BooleanTextFeature(feature_name, word)

    def get_feature_vector(self)-> List[Feature]:
        feature_vector = []
        document = self.queue.get_next_document()
        document_read = 0
        while document is not None:
            for feature in self.text_features:
                if not hasattr(document, feature):
                    continue
                value = getattr(document, feature)
                word_array = self.__split(value)
                for word in word_array:
                    word = self.__normalize(word)
                    word_feature = self.__get_feature_from_word(feature, word)
                    if word_feature is not None:
                        print("Feature found: %s" % word)
                        feature_vector.append(word_feature)
            document_read += 1
            document = self.queue.get_next_document()

        for name in self.numeric_features:
            characteristics = self.__load_numeric_characteristics(name)
            feature_vector.append(NumericFeature(name, characteristics))
        return feature_vector
