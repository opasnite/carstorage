from carstorage.learning.features.types.Feature import Feature


class BooleanTextFeature(Feature):

    def __init__(self, name: str, target: str) -> None:
        super().__init__(name)
        self.target = target

    def normalize(self, value) -> dict:
        norm = 0
        if self.target in value:
            norm = 1
        return {
            self.name + '_' + self.target: norm
        }

    def denormalize(self, value):
        return value

    def check_value_for_extremes(self, value) -> bool:
        return False
