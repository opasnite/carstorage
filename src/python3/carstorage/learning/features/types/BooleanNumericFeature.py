from carstorage.learning.features.types.Feature import Feature


class BooleanNumericFeature(Feature):
    def normalize(self, value) -> dict:
        return {
            self.name: value
        }

    def denormalize(self, value):
        return value

    def check_value_for_extremes(self, value) -> bool:
        return value < 0 or value > 1