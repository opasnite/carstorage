from abc import abstractmethod, ABC


class Feature(ABC):
    def __init__(self, name: str) -> None:
        self.name = name

    @abstractmethod
    def normalize(self, value) -> dict: raise NotImplementedError

    @abstractmethod
    def denormalize(self, value): raise NotImplementedError

    @abstractmethod
    def check_value_for_extremes(self, value) -> bool: raise NotImplementedError
