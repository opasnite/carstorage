from carstorage.learning.features.types.Feature import Feature
from carstorage.learning.features.types.NumericFeatureCharacteristics import NumericFeatureCharacteristics


class NumericFeature(Feature):

    def __init__(self, name: str, characteristics: NumericFeatureCharacteristics) -> None:
        super().__init__(name)
        self.characteristics = characteristics

    def __min_max_normalization(self, value: float):
        return (value - self.characteristics.minimum)/(self.characteristics.maximum - self.characteristics.minimum)

    def __min_max_denormalization(self, value: float):
        return value * (self.characteristics.maximum - self.characteristics.minimum) + self.characteristics.minimum;

    def normalize(self, value) -> dict:
        return {
            self.name: self.__min_max_normalization(value)
        }

    def denormalize(self, value):
        return self.__min_max_denormalization(value)

    def check_value_for_extremes(self, value) -> bool:
        return value < self.characteristics.minimum or value > self.characteristics.maximum