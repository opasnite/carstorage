class NumericFeatureCharacteristics:
    def __init__(self, first_quartile: float, third_quartile: float) -> None:
        self.first_quartile = first_quartile
        self.third_quartile = third_quartile
        self.IQR = third_quartile - first_quartile
        self.maximum = third_quartile + 2 * self.IQR
        self.minimum = first_quartile - 2 * self.IQR
        if self.minimum < 0:
            self.minimum = 0