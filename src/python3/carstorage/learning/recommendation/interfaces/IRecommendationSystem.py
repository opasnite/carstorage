from abc import ABC, abstractmethod
from typing import List

from carstorage.index.Document import Document


class IRecommendationSystem(ABC):
    @abstractmethod
    def find_common(self, document: Document, count: int = 5) -> List[Document]: raise NotImplementedError
