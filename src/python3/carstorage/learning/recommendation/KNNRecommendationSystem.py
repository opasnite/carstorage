from typing import List

from scipy.spatial.distance import euclidean

from carstorage.index.Document import Document
from carstorage.index.DocumentQueue import DocumentQueue
from carstorage.learning.features.types.Feature import Feature
from carstorage.learning.recommendation.interfaces.IRecommendationSystem import IRecommendationSystem
from carstorage.learning.utils.DocumentHelper import DocumentHelper


class KNNRecommendationSystem(IRecommendationSystem):
    def __init__(self, elastic_connection: str, features: List[Feature]) -> None:
        self.connection = elastic_connection
        self.doc_helper = DocumentHelper(features.copy())
        self.__features = features
        self.feature_count = len(features)

    def find_common(self, target: Document, count: int = 5) -> List[Document]:
        converted_target = self.doc_helper.convert_for_clustering(target)
        common_list = []
        if len(converted_target) <= 0:
            return common_list
        queue = DocumentQueue(self.connection, target.cluster)
        document = queue.get_next_document()
        document_read = 0
        while document is not None:
            if document.unique_id == target.unique_id:
                document = queue.get_next_document()
                continue
            converted_document = self.doc_helper.convert_for_clustering(document)
            if len(converted_document) <= 0:
                document = queue.get_next_document()
                continue
            distance = euclidean(converted_document, converted_target)
            common_list.append((distance, document))
            common_list.sort(key=lambda x: x[0])
            if len(common_list) > count:
                del common_list[count - 1:len(common_list) - 1]
            document_read += 1
            document = queue.get_next_document()
        resulting_list = []
        for pair in common_list:
            resulting_list.append(pair[1])
        return resulting_list

