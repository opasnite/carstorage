from typing import Union, Tuple, List

from carstorage.index.Document import Document
from carstorage.learning.features.types.Feature import Feature


class DocumentHelper:
    def __init__(self, features: List[Feature]) -> None:
        self.__features = features

    def convert_for_regression(self, document: Document, target_feature: str) -> Tuple[list, list]:
        doc_vars = vars(document)
        converted = []
        target = []
        for i in range(0, len(self.__features)):
            feature = self.__features[i]
            if feature.name in doc_vars:
                value = doc_vars[feature.name]
            else:
                return [], []
            if feature.check_value_for_extremes(value):
                return [], []
            value_list = feature.normalize(value)
            if feature.name == target_feature:
                for item in value_list.values():
                    target.append(item)
            else:
                for item in value_list.values():
                    converted.append(item)
        return converted, target

    def convert_for_clustering(self, document: Document) -> list:
        doc_vars = vars(document)
        converted = []
        for i in range(0, len(self.__features)):
            feature = self.__features[i]
            if feature.name in doc_vars:
                value = doc_vars[feature.name]
            else:
                return []
            if feature.check_value_for_extremes(value):
                return []
            value_list = feature.normalize(value)
            for item in value_list.values():
                converted.append(item)
        return converted

