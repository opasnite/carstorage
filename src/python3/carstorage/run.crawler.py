from multiprocessing.pool import Pool
from time import sleep
from uuid import UUID

from carstorage.crawler.CarStorageCrawler import CarStorageCrawler

crawler_id = '5a84baf2-8ea8-4481-8809-027589255f81'
server_url = 'http://carstorage.tk:8080'


def start_crawler(number: int):
    #print("Starting crawler instance %d!" % number)
    crawler = CarStorageCrawler(server_url, UUID(crawler_id))
    sleep(number * 0.1)
    try:
        crawler.do_next_job()
    except Exception as e:
        print("Failed to execute job! ", flush=False)
        print(e)
    #print("Closing crawler instance %d!" % number)


if __name__ == '__main__':
    while True:
        try:
            with Pool(processes=4) as pool:
                pool.map(start_crawler, range(1, 5))
                pool.close()
                pool.join()
            sleep(0.3)
        except KeyboardInterrupt:
            break


