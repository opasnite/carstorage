from carstorage.learning.clustering.FacilityProblemIndexClustering import FacilityProblemIndexClustering
from carstorage.storage.CacheProvider import CacheProvider

elastic = 'http://localhost:9200'

if __name__ == '__main__':
    provider = CacheProvider('var/cache')
    features = provider.get('features')
    if features is None:
        raise Exception("Features must be extracted before learning models!")
    model = FacilityProblemIndexClustering(elastic, features)
    old_centroids = provider.get('centroids')
    centroids = model.run_clustering(old_centroids)
    print(centroids)
    provider.put('centroids', centroids)


