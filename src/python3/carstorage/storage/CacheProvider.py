import pickle
from os.path import exists


class CacheProvider:
    def __init__(self, base_directory: str) -> None:
        self.base_directory = base_directory

    def __get_path(self, key: str):
        return self.base_directory + "/data_" + key + '.bin'

    def put(self, key: str, value):
        path = self.__get_path(key)
        binary_file = open(path, mode='wb+')
        try:
            pickle.dump(value, binary_file)
        finally:
            binary_file.close()

    def get(self, key: str):
        path = self.__get_path(key)
        if exists(path):
            binary_file = open(path, mode='rb')
            try:
                value = pickle.load(binary_file)
            finally:
                binary_file.close()
            return value
        else:
            return None
