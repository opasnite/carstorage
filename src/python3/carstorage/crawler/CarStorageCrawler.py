from uuid import UUID

from easy_search.core.crawler.process.Crawler import Crawler
from easy_search.core.crawler.utils.communication.http.DocumentCommunicator import DocumentCommunicator
from easy_search.core.crawler.utils.communication.http.JobCommunicator import JobCommunicator

from carstorage.crawler.plugin.CarsBgPlugin import CarsBgPlugin


class CarStorageCrawler(Crawler):
    def __init__(self, server_url: str, crawler_id: UUID) -> None:
        plugins = {
            'cars.bg': CarsBgPlugin()
        }
        super().__init__(plugins,
                         JobCommunicator(server_url, crawler_id),
                         DocumentCommunicator(server_url, crawler_id))
