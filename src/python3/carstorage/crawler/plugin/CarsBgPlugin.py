from datetime import date, datetime
from pprint import pprint
from typing import Tuple, List
from uuid import UUID, uuid4

from easy_search.core.crawler.process.plugin.BaseHTTPCrawlerPlugin import BaseHTTPCrawlerPlugin
from easy_search.interfaces.base.enum.JobType import JobType
from easy_search.interfaces.base.job.JobDescription import JobDescription
from easy_search.interfaces.server.index.communication.common.IndexDocument import IndexDocument
from lxml import html
from requests import Response
from sklearn.cluster import KMeans

from carstorage.crawler.plugin.exception.UnexpectedStructureException import UnexpectedStructureException
from carstorage.index.Document import Document
from carstorage.learning.clustering.FacilityProblemIndexClustering import FacilityProblemIndexClustering
from carstorage.learning.regression.IndexSVR import IndexSVR
from carstorage.learning.utils.DocumentHelper import DocumentHelper
from carstorage.storage.CacheProvider import CacheProvider


class CarsBgPlugin(BaseHTTPCrawlerPlugin):
    month_mapping = [('Януари', '01'),
                     ('Февруари', '02'),
                     ('Март', '03'),
                     ('Април', '04'),
                     ('Май', '05'),
                     ('Юни', '06'),
                     ('Юли', '07'),
                     ('Август', '08'),
                     ('Септември', '09'),
                     ('Октомври', '10'),
                     ('Ноември', '11'),
                     ('Декември', '12')]

    def __init__(self, agent: str = 'easy_search Crawler Bot') -> None:
        super().__init__(agent)
        self.cache_helper = CacheProvider("var/cache")

    def parse_response(self, response: Response, job_type: JobType) -> Tuple[List[IndexDocument], List[JobDescription]]:
        print("Parsing: " + response.url)
        tree = html.document_fromstring(response.content)
        if job_type == JobType.HARVEST:
            return [], self.__harvest(tree)
        else:
            return self.__extract(tree, response.url), []

    def __find(self, tree, selector):
        tlist = tree.cssselect(selector)
        if tlist is None or tlist == []:
            raise UnexpectedStructureException("Element not found!")
        return tlist

    def __find_one(self, tree, selectr, position = 0):
        tlist = self.__find(tree, selectr)
        if len(tlist) < position + 1:
            raise UnexpectedStructureException("Element not found!")
        return tlist[position]

    def __replace_month(self, date_string: str) -> str:
        for k, v in self.month_mapping:
            date_string = date_string.replace(k.lower(), v)
        return date_string

    def __harvest(self, tree) -> List[JobDescription]:
        form = self.__find_one(tree, '#carsForm')
        next_page_links = form.cssselect("table table table.ver13black:first-of-type a")
        jobs = []
        for link in next_page_links:
            job_url = link.base + link.attrib['href']
            job_url = job_url.replace("/&+/", "&")
            if len(job_url) > 0:
                #print("Found harvest job: " + job_url)
                jobs.append(JobDescription(JobType.HARVEST, job_url, 'cars.bg'))
        car_links = form.cssselect("table table table.tableListResults a.ver15black")
        for link in car_links:
            job_url = link.base + link.attrib['href']
            job_url = job_url.replace("/&+/", "&")
            if len(job_url) > 0:
                #print("Found extract job: " + job_url)
                jobs.append(JobDescription(JobType.EXTRACT, job_url, 'cars.bg'))
        return jobs

    def __extract(self, tree, job_url: str) -> List[IndexDocument]:
        body = self.__find_one(tree, "body")
        main = self.__find_one(body, "table.ver13black")
        main_children = list(main)
        title = str(main_children[0].text_content()).strip()
        price_block = self.__find_one(
            self.__find_one(
                main_children[1],
                'table'
            ), 'td', 1)
        price = str(self.__find_one(price_block, 'strong').text_content()).strip()
        currency = str(price_block.text_content()).replace(price, '').replace('/[\n\t]/', '').strip()
        price = price.replace(",", "")
        highlights_container = main_children[2]
        highlights_inner = self.__find_one(highlights_container, 'table')
        highlights_tables = self.__find(highlights_inner, 'table')
        keywords_check = []
        keywords = []
        key = -1
        for highlights in highlights_tables:
            key += 1
            if key == 0:
                continue
            rows = self.__find(highlights, 'tr')
            for row in rows:
                keyword = str(row.text_content()).replace('/[\n\t]/', '').strip()
                khash = self.hash_generator.generate_target_hash(keyword)
                if khash not in keywords_check:
                    keywords_check.append(khash)
                    keywords.append(keyword.lower())
        ddate = datetime.strptime(self.__replace_month(keywords[0]), '%m %Y')
        kilometers = keywords[1].replace('км', '').replace(',', '').strip()
        extras_container = main_children[3]
        extras_inner = self.__find_one(extras_container, 'table')
        extras_rows = self.__find(extras_inner, 'tr')
        key = -1
        for row in extras_rows:
            key += 1
            if key < 2:
                continue
            keyword_list = str(self.__find_one(row, 'td', 1).text_content()).replace('/[\n\t]/', '').strip()
            for keyword in keyword_list.split(','):
                keyword = keyword.strip()
                khash = self.hash_generator.generate_target_hash(keyword)
                if khash not in keywords_check:
                    keywords_check.append(khash)
                    keywords.append(keyword.lower())
        description_container = main_children[4]
        description_inner = self.__find_one(description_container, 'table')
        description_rows = list(description_inner)
        if len(description_rows) >= 3:
            description_row = description_rows[2]
            description = str(description_row.text_content()).strip()
        else:
            description = ''
        document = Document(self.hash_generator.generate_target_hash(job_url), title, job_url, description,
                            float(price), currency, ddate, int(kilometers))
        document.add_keywords(keywords)

        features = self.cache_helper.get('features')
        if features is not None:
            doc_helper = DocumentHelper(features)
            centroids = self.cache_helper.get('centroids')
            if centroids is not None:
                converted_document = doc_helper.convert_for_clustering(document)
                if len(converted_document) > 0:
                    clustering = KMeans(len(centroids))
                    clustering.cluster_centers_ = centroids
                    document_cluster = clustering.predict([converted_document])
                    document.cluster = document_cluster[0]
            regression_model = self.cache_helper.get('regression_model')
            if regression_model is not None:
                converted_document, target = doc_helper.convert_for_regression(document, 'price')
                if len(converted_document) > 0:
                    predicted_price = regression_model.predict([converted_document])
                    predicted_price = predicted_price[0]
                    for feature in features:
                        if feature.name == 'price':
                            predicted_price = feature.denormalize(predicted_price)
                            break
                    document.predicted_price = predicted_price
        return [document]



