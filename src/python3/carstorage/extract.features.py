from elasticsearch import Elasticsearch
import pickle
from carstorage.learning.features.extraction.IndexFeatureExtraction import IndexFeatureExtractor
from carstorage.storage.CacheProvider import CacheProvider

elastic = 'http://localhost:9200'

if __name__ == '__main__':
    extractor = IndexFeatureExtractor(elastic, 0.14)
    extractor.add_numeric_feature('kilometers')
    extractor.add_numeric_feature('price')
    extractor.add_text_feature('keywords')
    features = extractor.get_feature_vector()

    provider = CacheProvider("var/cache")
    provider.put("features", features)


