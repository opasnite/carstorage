from carstorage.index.DocumentQueue import DocumentQueue
from carstorage.learning.recommendation.KNNRecommendationSystem import KNNRecommendationSystem
from carstorage.storage.CacheProvider import CacheProvider

elastic = 'http://localhost:9200'

if __name__ == '__main__':
    provider = CacheProvider('var/cache')
    features = provider.get('features')
    if features is None:
        raise Exception("Features must be extracted before learning models!")
    model = KNNRecommendationSystem(elastic, features)
    queue = DocumentQueue(elastic, 1)
    document = queue.get_next_document()
    if document is None:
        raise Exception("No documents available!")
    print(vars(document))
    commons = model.find_common(document)
    print("Commons:")
    for doc in commons:
        print(vars(doc))
