from datetime import datetime
from typing import Type
from uuid import UUID

from easy_search.core.server.index.manager.elasticsearch.ElasticDocumentManager import ElasticDocumentManager
from easy_search.core.server.job.data.mongodb.SearchEngineContext import SearchEngineContext
from easy_search.core.server.rest.flask.FlaskServer import FlaskServer

from carstorage.index.Document import Document
from easy_search.interfaces.base.enum.JobType import JobType
from easy_search.interfaces.server.job.data.data.JobData import JobData


class Server(FlaskServer):
    def __init__(self, context_connection: str, manager_connection: str) -> None:
        self.context = SearchEngineContext(context_connection, 'carstorage_server')
        super().__init__('carstorage_server', Document, self.context,
                         ElasticDocumentManager(manager_connection, 'carstorage_server'))

    def initialize(self):
        c_id = UUID("5a84baf2-8ea8-4481-8809-027589255f81")
        has = self.context.crawler_set().exists(c_id)
        if not has:
            entity = {"date_added": datetime.now(), "allowed_ip": ['127.0.0.1'],
                      "crawler_id": c_id.__str__(), "last_call": None}
            self.context.crawler_set()\
                .get_collection()\
                .insert_one(entity)
        has = self.context.job_set().get_collection().count() > 0
        if not has:
            job = JobData(JobType.HARVEST, 'https://www.cars.bg/?go=cars&search=1&advanced=&fromhomeu=1&currencyId=1&'
                                           'yearTo=&autotype=1&stateId=1&section=home&categoryId=0&doorId=0&brandId=0&'
                                           'modelId=0&fuelId=0&gearId=0&yearFrom=&priceFrom=&priceTo=&man_priceFrom=&'
                                           'man_priceTo=&regionId=0&offersFor4=1&offersFor1=1&filterOrderBy=1',
                          False, c_id, 'cars.bg')
            self.context.job_set().add(job)
