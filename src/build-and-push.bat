cd ~dp0
docker stack rm carstorage
cd app
docker build -t carstorage-server .
docker tag carstorage-server kiposolutions/carstorage-server:1.0
docker push kiposolutions/carstorage-server:1.0 
cd ../public
docker build -t carstorage-web .
docker tag carstorage-web kiposolutions/carstorage-web:1.0
docker push kiposolutions/carstorage-web:1.0 
cd ../
docker stack deploy -c docker-compose.yml carstorage